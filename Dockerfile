# Start: Debian version of the Maubot Docker container
FROM node:16 AS frontend-builder

COPY maubot/maubot/management/frontend /frontend
RUN cd /frontend && yarn --prod && yarn build

FROM debian:11-slim
ENV DEBIAN_FRONTEND=noninteractive
RUN apt update && apt install -y python3 python3-pip python3-setuptools python3-wheel \
    ca-certificates python3-cffi python3-lxml python3-olm python3-future python3-sqlalchemy python3-click python3-bcrypt\
     python3-regex python3-pycryptodome python3-pil python3-magic python3-feedparser python3-dateutil python3-tz\
     python3-markdown python3 gcc g++ && rm -rf /var/lib/apt/lists/* && rm -rf /var/lib/apt/lists/*

COPY maubot/requirements.txt /opt/maubot/requirements.txt
COPY maubot/optional-requirements.txt /opt/maubot/optional-requirements.txt
WORKDIR /opt/maubot
RUN pip3 install -r requirements.txt -r optional-requirements.txt dateparser langdetect python-gitlab pyquery cchardet \
    semver tzlocal cssselect

COPY maubot /opt/maubot
RUN cp maubot/example-config.yaml .
COPY maubot/docker/mbc.sh /usr/local/bin/mbc
COPY --from=frontend-builder /frontend/build /opt/maubot/frontend
ENV UID=1337 GID=1337 XDG_CONFIG_HOME=/data
VOLUME /data

# Now the dependencies I need
RUN python3 -m pip install maubot>=0.3.1 OpenAIAuth>=0.0.3.1

WORKDIR /opt/maubot
RUN sed -i '1 s/sh/bash/' /opt/maubot/docker/run.sh
RUN sed -i 's/su-exec $UID:$GID//' /opt/maubot/docker/run.sh
RUN apt update && apt install -y wget && rm -rf /var/lib/apt/lists/*
RUN wget https://github.com/mikefarah/yq/releases/download/v4.30.5/yq_linux_amd64 -O /usr/local/bin/yq && chmod 755 /usr/local/bin/yq

RUN groupadd -g 1337 1337
RUN useradd -u 1337 -g 1337 -s /bin/sh -m 1337 # <--- the '-m' create a user home directory
RUN /opt/maubot/docker/run.sh

USER 1337
CMD ["/opt/maubot/docker/run.sh"]
