# maubot-gpt
## A ChatGPT client for Maubot

![Screenshot of the bot in action](doc/screenshot.webp)

### What is ChatGPT?
Go check it out [over here](https://chat.openai.com/chat)!

### What is Maubot?
Read about it [over here](https://docs.mau.fi/maubot/usage/index.html)!

### How stable is this?
This has the official Works On My Machine seal of approval. Nothing
is guaranteed.

### How do I use this?
This guide assumes knowledge of `docker-compose` and Python. It also requires
an OpenAI account with ChatGPT access.

To use this tool, you need to run a Maubot server. Sadly, I couldn't get
this to work on the official Maubot package, so I've had to modify the Docker
container a bit to install all the dependencies.

First, instead of running the normal Maubot server, we need to configure the
custom server. To do this, you can try these commands:

1. `git clone --recurse-submodules https://gitlab.com/jeroenhd/chatgpt-maubot.git`
2. `cd chatgpt-maubot`
3. `docker-compose build`
4. `docker-compose up -d`

This should run the Maubot server. You can copy over your configuration and
data from an existing install or create a new configuration; the setup procedure
can be found here: https://docs.mau.fi/maubot/usage/setup/index.html

Now that you have a compatible server running, we need to build the bot.
Commands to do so:

1. `python3 -m venv virtualenv`
2. `source virtualenv/bin/activate`
3. `python3 -m pip install -r requirements.txt`
4. `mbc build`

This should produce a nice .mbp file for you containing the bot code.

Next, you need to upload the bot to Maubot. You can follow
[the offical](https://docs.mau.fi/maubot/usage/basic.html) guide for this.

Finally, you need to configure the bot. Log in to the Maubot manager in
whatever way you prefer and do the following (use the Maubot docs for
details):

1. Create a client
2. Upload the plugin (.mbp)
3. Set up an instance
4. Configure the instance

There are three fields that can be configured at the moment. You will find
dummy variables ready for you after uploading the bot. These fields are:

- email: the email address for your OpenAI account
- password: the password for your OpenAI account
- whitelist: a list of Matrix IDs of users that are permitted 
to talk to the bot

You need to fill in the email and password field at the moment. Token support
is planned but not yet implemented (who knows if it ever will be).

After filling in these details, invite your bot for a chat. You can use the
following commands:

- `!me`: send a prompt to the bot
- `!ping`: check if the bot is still operational
- `!reset`: reset the conversation and start over
- `!reconnect`: reconnect top ChatGPT if the connection died for some reason

### Warning: I do not recommend using this bot in a public room.
Conversations are shared between users and ChatGPT might ban your account.

## FAQ
### Why am I getting errors about dependencies?
You're probably running the official Maubot image. This is currently not
supported. Use the custom Dockerfile to get around this.

### Why is the chatbot so slow?
ChatGPT isn't very fast sometimes! Be patient and give the bot a minute to
start after starting the server/reconnecting the bot.

### What's with the completely custom Docker image?
The official Maubot image relies on Alpine but the library this bot is
written around has a dependency that doesn't work on Alpine because of a
Go compiler issue.

This Docker image is based on the [official version](https://mau.dev/maubot/maubot/-/blob/master/Dockerfile)
but was rewritten for Debian + glibc support to make the dependencies work.

### Why doesn't this run on ARM?
I don't have access to an ARM device (nor the interest to be honest) to test
proper ARM Docker support. Pull requests are welcome. Compatibility shouldn't
be too hard to accomplish, I just don't know how.

### Why did you copy over the entire source file for ChatGPT?
The original library uses `requests` rather than `aiohttp` for HTTP
communication and that's not very efficient for Maubot. It also reduces
the amount of transitive dependencies.

### Can you give me a license that doesn't require me to open source my code?
Sorry, the two projects that do most of the heavy lifting are GPL-2 or
GPL-3 licensed. Feel free to write your own bot or pick another if this 
is a problem for you.