from __future__ import annotations

import json
import uuid
from asyncio import AbstractEventLoop
from typing import Type

from OpenAIAuth.OpenAIAuth import OpenAIAuth, Debugger
from aiohttp import ClientSession
from maubot import Plugin, PluginWebApp
from maubot.handlers import command
from maubot.loader import BasePluginLoader
from maubot.matrix import MaubotMatrixClient
from mautrix.types import MessageType, MessageEvent, TextMessageEventContent, Format
from mautrix.util.config import ConfigUpdateHelper, BaseProxyConfig
from mautrix.util.logging import TraceLogger
from sqlalchemy.engine import Engine


class GptChatBotConfig(BaseProxyConfig):
    def do_update(self, helper: ConfigUpdateHelper) -> None:
        helper.copy('email')
        helper.copy('password')
        helper.copy('whitelist')


class GptChatBot(Plugin):
    chatbot: Chatbot | None
    conversation_id: str | None

    def __init__(
            self,
            client: MaubotMatrixClient,
            loop: AbstractEventLoop,
            http: ClientSession,
            instance_id: str,
            log: TraceLogger,
            config: BaseProxyConfig | None,
            database: Engine | None,
            webapp: PluginWebApp | None,
            webapp_url: str | None,
            loader: BasePluginLoader,
    ):
        super().__init__(client, loop, http, instance_id, log, config, database, webapp, webapp_url, loader)
        self.chatbot = None
        self.conversation_id = None

    async def start(self) -> None:
        self.log.info('Starting webhook bot!')

        self.log.info('Loading config...')
        self.config.load_and_update()

        self.log.info('Connecting to ChatGPT...')

        await self.initialise_chatbot()
        self.log.info('Initialised!')

    async def initialise_chatbot(self):
        auth_config = {'email': self.config.get('email', ''), 'password': self.config.get('password', '')}
        self.chatbot = Chatbot(config=auth_config, conversation_id=None, http=self.http)

        # Reset after restart
        self.chatbot.reset_chat()
        # Ensure connectivity
        e = await self.chatbot.refresh_session()
        if e is not None:
            self.log.error('Failed to refresh chat session: %s' % e)
            self.chatbot = None

    @classmethod
    def get_config_class(cls) -> Type['BaseProxyConfig']:
        return GptChatBotConfig

    @command.new("ping", help="Check chatbot status")
    async def ping_handler(self, evt: MessageEvent) -> None:
        self.log.info("Received ping")
        if self.validate_user_in_whitelist(evt.sender):
            self.log.info('Ping is authorized')
            if self.chatbot is None:
                await evt.respond(TextMessageEventContent(
                    msgtype=MessageType.NOTICE,
                    format=Format.HTML,
                    body=f"{evt.sender}: Chatbot is down!",
                    formatted_body=f"<a href='https://matrix.to/#/{evt.sender}'>{evt.sender}</a>: Chatbot is down!"
                ))
            else:
                await evt.respond(TextMessageEventContent(
                    msgtype=MessageType.NOTICE,
                    format=Format.HTML,
                    body=f"{evt.sender}: Chatbot is up!",
                    formatted_body=f"<a href='https://matrix.to/#/{evt.sender}'>{evt.sender}</a>: Chatbot is up!"
                ))
        else:
            self.log.error('Unauthorized ping from %s' % evt.sender)
            await evt.respond(TextMessageEventContent(
                msgtype=MessageType.NOTICE,
                format=Format.HTML,
                body=f"{evt.sender}: You are not authorised!",
                formatted_body=f"<a href='https://matrix.to/#/{evt.sender}'>{evt.sender}</a>: You are not authorised!"
            ))

    @command.new("reset", help="Reset")
    async def reset_handler(self, evt: MessageEvent) -> None:
        if self.validate_user_in_whitelist(evt.sender):
            self.log.info('Resetting conversation!')
            if self.chatbot is None:
                await evt.respond(TextMessageEventContent(
                    msgtype=MessageType.NOTICE,
                    format=Format.HTML,
                    body=f"{evt.sender}: Chatbot is down!",
                    formatted_body=f"<a href='https://matrix.to/#/{evt.sender}'>{evt.sender}</a>: Chatbot is down!"
                ))
            else:
                self.chatbot.reset_chat()
                await evt.respond(TextMessageEventContent(
                    msgtype=MessageType.NOTICE,
                    format=Format.HTML,
                    body=f"{evt.sender}: conversation ID was reset!",
                    formatted_body=f"<a href='https://matrix.to/#/{evt.sender}'>{evt.sender}</a>: conversation ID was reset!"
                ))
        else:
            self.log.error('Unauthorized reset from %s' % evt.sender)

    @command.new("reconnect", help="Reconnect the chatbot")
    async def reconnect_handler(self, evt: MessageEvent) -> None:
        if self.validate_user_in_whitelist(evt.sender):
            self.log.info('Reconnecting chatbot!')
            await self.initialise_chatbot()
            await evt.respond(TextMessageEventContent(
                msgtype=MessageType.NOTICE,
                format=Format.HTML,
                body=f"{evt.sender}: Chatbot is being initialised!!",
                formatted_body=f"<a href='https://matrix.to/#/{evt.sender}'>{evt.sender}</a>: Chatbot is being initialised!"
            ))
        else:
            self.log.error('Unauthorized reconnect from %s' % evt.sender)

    @command.new("me", help="Talk to the chatbot")
    @command.argument(name="message", pass_raw=True, required=True)
    async def query(self, evt: MessageEvent, message: str = "") -> None:
        if self.validate_user_in_whitelist(evt.sender):
            if len(message) == 0:
                self.log.error(f'Empty query from {evt.sender}, ignoring!')
                return

            self.log.info('Submitting query!')
            response = await self.chatbot.get_chat_response(message, output="text")
            self.conversation_id = response['conversation_id']
            await evt.respond(TextMessageEventContent(
                msgtype=MessageType.NOTICE,
                format=Format.HTML,
                body=f"{evt.sender}: {response['message']}",
                formatted_body=f"<a href='https://matrix.to/#/{evt.sender}'>{evt.sender}</a>: {response['message']}"
            ))
        else:
            self.log.error('Unauthorised message from %s' % evt.sender)

    def validate_user_in_whitelist(self, username: str) -> bool:
        return username in self.config.get('whitelist', [])


# Code ported from: https://github.com/acheong08/ChatGPT/blob/main/src/revChatGPT/revChatGPT.py

def generate_uuid() -> str:
    uid = str(uuid.uuid4())
    return uid


class Chatbot:
    config: json
    conversation_id: str | None
    parent_id: str | None
    headers: dict
    conversation_id_prev: str | None
    parent_id_prev: str | None
    http: ClientSession

    def __init__(self, config, http, conversation_id=None, debug=False, refresh=True):
        self.debugger = Debugger(debug)
        self.debug = debug
        self.config = config
        self.conversation_id = conversation_id
        self.http = http
        self.parent_id = generate_uuid()
        if "session_token" in config or ("email" in config and "password" in config) and refresh:
            self.refresh_session()
        if "Authorization" in config:
            self.refresh_headers()

    # Resets the conversation ID and parent ID
    def reset_chat(self) -> None:
        self.conversation_id = None
        self.parent_id = generate_uuid()

    # Refreshes the headers -- Internal use only
    def refresh_headers(self) -> None:
        if "Authorization" not in self.config:
            self.config["Authorization"] = ""
        elif self.config["Authorization"] is None:
            self.config["Authorization"] = ""
        self.headers = {
            "Host": "chat.openai.com",
            "Accept": "text/event-stream",
            "Authorization": "Bearer " + self.config["Authorization"],
            "Content-Type": "application/json",
            "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) "
                          "Version/16.1 Safari/605.1.15",
            "X-Openai-Assistant-App-Id": "",
            "Connection": "close",
            "Accept-Language": "en-US,en;q=0.9",
            "Referer": "https://chat.openai.com/chat",
        }

    # Generates a UUID -- Internal use only

    # Generator for chat stream -- Internal use only
    async def get_chat_stream(self, data) -> None:
        response = await self.http.post("https://chat.openai.com/backend-api/conversation",
                                        headers=self.headers,
                                        data=json.dumps(data),
                                        timeout=100)

        text = await response.text()
        for line in text.splitlines():
            try:
                # line = line.decode("utf-8")
                if line == "":
                    continue
                line = line[6:]
                line = json.loads(line)
                try:
                    message = line["message"]["content"]["parts"][0]
                    self.conversation_id = line["conversation_id"]
                    self.parent_id = line["message"]["id"]
                except KeyError:
                    continue
                yield {
                    "message": message,
                    "conversation_id": self.conversation_id,
                    "parent_id": self.parent_id,
                }
            except Exception as e:
                # print(f"Unknown exception {e}")
                continue

    # Gets the chat response as text -- Internal use only
    async def get_chat_text(self, data) -> dict:
        # set headers
        for header, value in self.headers.items():
            self.http.headers.add(header, value)

        # Set multiple cookies
        cookies = {}
        if "session_token" in self.config:
            cookies["__Secure-next-auth.session-token"] = self.config["session_token"]

        cookies["__Secure-next-auth.callback-url"] = "https://chat.openai.com/"
        self.http.cookie_jar.update_cookies(cookies)

        # Not supported rn
        # # Set proxies
        # if self.config.get("proxy", "") != "":
        #     s.proxies = {
        #         "http": self.config["proxy"],
        #         "https": self.config["proxy"],
        #     }

        response = await self.http.post("https://chat.openai.com/backend-api/conversation", data=json.dumps(data))

        try:
            response = (await response.text()).splitlines()[-4]
            response = response[6:]
        except Exception as exc:
            self.debugger.log("Incorrect response from OpenAI API")
            self.debugger.log(await response.text())
            try:
                resp = await response.json()
                if resp['detail']['code'] == "invalid_api_key":
                    if "email" in self.config and "password" in self.config:
                        await self.refresh_session()
                        return await self.get_chat_text(data)
                    else:
                        raise Exception(
                            "Missing necessary credentials") from exc
            except Exception as exc2:
                raise Exception("Not a JSON response") from exc2
            raise Exception("Incorrect response from OpenAI API") from exc
        response = json.loads(response)
        self.parent_id = response["message"]["id"]
        self.conversation_id = response["conversation_id"]
        message = response["message"]["content"]["parts"][0]
        return {
            "message": message,
            "conversation_id": self.conversation_id,
            "parent_id": self.parent_id,
        }

    # Gets the chat response
    async def get_chat_response(self, prompt, output="text") -> dict or None:
        data = {
            "action": "next",
            "messages": [
                {
                    "id": str(generate_uuid()),
                    "role": "user",
                    "content": {"content_type": "text", "parts": [prompt]},
                },
            ],
            "conversation_id": self.conversation_id,
            "parent_message_id": self.parent_id,
            "model": "text-davinci-002-render",
        }
        self.conversation_id_prev = self.conversation_id
        self.parent_id_prev = self.parent_id
        if output == "text":
            return await self.get_chat_text(data)
        elif output == "stream":
            return self.get_chat_stream(data)
        else:
            raise ValueError("Output must be either 'text' or 'stream'")

    def rollback_conversation(self) -> None:
        self.conversation_id = self.conversation_id_prev
        self.parent_id = self.parent_id_prev

    async def refresh_session(self):
        if (
                "session_token" not in self.config
                and ("email" not in self.config or "password" not in self.config)
                and "Authorization" not in self.config
        ):
            error = ValueError("No tokens provided")
            self.debugger.log(f"{error}")
            raise error
        elif "session_token" in self.config:
            if (
                    self.config["session_token"] is None
                    or self.config["session_token"] == ""
            ):
                raise ValueError("No tokens provided")

            # Not supported rn
            # if self.config.get("proxy", "") != "":
            #     s.proxies = {
            #         "http": self.config["proxy"],
            #         "https": self.config["proxy"],
            #     }

            # Set cookies
            self.http.cookie_jar.update_cookies({"__Secure-next-auth.session-token": self.config["session_token"]})
            response = await self.http.get(
                "https://chat.openai.com/api/auth/session",
                headers={
                    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, "
                                  "like Gecko) Version/16.1 Safari/605.1.15 ",
                },
            )
            try:
                self.config["session_token"] = response.cookies.get(
                    "__Secure-next-auth.session-token",
                )
                self.config["Authorization"] = (await response.json())["accessToken"]
                self.refresh_headers()
            except Exception as exc:
                print("Error refreshing session")
                self.debugger.log(await response.text())
                raise Exception("Error refreshing session") from exc
        elif "email" in self.config and "password" in self.config:
            try:
                self.login(self.config["email"], self.config["password"])
            except Exception as exc:
                self.debugger.log("Login failed")
                raise exc
        elif "Authorization" in self.config:
            self.refresh_headers()
            return
        else:
            raise ValueError("No tokens provided")

    def login(self, email, password) -> None:
        self.debugger.log("Logging in...")
        use_proxy = False
        proxy = None
        if "proxy" in self.config:
            if self.config["proxy"] != "":
                use_proxy = True
                proxy = self.config["proxy"]
        auth = OpenAIAuth(email, password, use_proxy, proxy, debug=self.debug)
        try:
            auth.begin()
        except Exception as exc:
            # if ValueError with e as "Captcha detected" fail
            if exc == "Captcha detected":
                self.debugger.log(
                    "Captcha not supported. Use session tokens instead.")
                raise ValueError("Captcha detected") from exc
            raise exc
        if auth.access_token is not None:
            self.config["Authorization"] = auth.access_token
            if auth.session_token is not None:
                self.config["session_token"] = auth.session_token
            else:
                possible_tokens = auth.session.cookies.get(
                    "__Secure-next-auth.session-token",
                )
                if possible_tokens is not None:
                    if len(possible_tokens) > 1:
                        self.config["session_token"] = possible_tokens[0]
                    else:
                        try:
                            self.config["session_token"] = possible_tokens
                        except Exception as exc:
                            raise Exception("Error logging in") from exc
            self.refresh_headers()
        else:
            raise Exception("Error logging in")
